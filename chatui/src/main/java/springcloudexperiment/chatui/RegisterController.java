package springcloudexperiment.chatui;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import springcloudexperiment.chatui.security.AuthenticationRequest;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * @author Robin Engel
 */
@Controller
public class RegisterController {

    private final RestTemplate restTemplate;

    @Autowired
    public RegisterController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }


    @RequestMapping(path = "/register", method = GET)
    public String register(Model model) {
        return "register";
    }

    @RequestMapping(path = "/register", method = POST)
    public String postRegister(@RequestBody MultiValueMap<String, String> formdata, Model model) {
        try {
            String username = formdata.get("username").get(0);
            String password = formdata.get("password").get(0);

            postNewUser(new User(username));
            postNewPassword(username, password);

            return "login";
        } catch (Exception e) {
            model.addAttribute("error", e.getMessage());
            return "register";
        }
    }

    private void postNewUser(User user) {
        restTemplate.postForLocation("http://user-service/users", user);
    }

    private void postNewPassword(String username, String password) {
        AuthenticationRequest authenticationRequest =
                new AuthenticationRequest(username, password);
        restTemplate.postForLocation("http://authentication-service/register", authenticationRequest);
    }
}

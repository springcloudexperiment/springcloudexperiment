package springcloudexperiment.chatui.security;

public class AuthenticationResult {

    private boolean successful;

    public AuthenticationResult() {
    }

    public AuthenticationResult(boolean successful) {
        this.successful = successful;
    }

    public boolean isSuccessful() {
        return successful;
    }
}

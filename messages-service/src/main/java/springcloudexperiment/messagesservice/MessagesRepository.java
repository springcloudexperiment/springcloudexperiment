package springcloudexperiment.messagesservice;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "messages")
public interface MessagesRepository extends CrudRepository<Message, String> {


}

package springcloudexperiment.chatui.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import springcloudexperiment.chatui.User;

import java.util.Collections;
import java.util.Optional;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Component
public class ChatUiAuthenticationProvider implements AuthenticationProvider {

    private final RestTemplate restTemplate;

    @Autowired
    public ChatUiAuthenticationProvider(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName().trim();
        String password = authentication.getCredentials().toString().trim();

        return getUser(username)
                .filter(u -> checkPassword(u.getName(), password))
                .map(u -> (Authentication)
                        new ChatUiAuthentication(
                                u, password,
                                Collections.singletonList(new SimpleGrantedAuthority("user"))))
                .orElse(null);
    }

    private Optional<User> getUser(String username) {
        try {
            ResponseEntity<User> response = restTemplate.exchange(
                    "http://user-service/users/search/findByName?name={name}",
                    GET,
                    null,
                    new ParameterizedTypeReference<User>() {
                    },
                    username);

            return Optional.of(response.getBody());
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode() == NOT_FOUND) {
                return Optional.empty();
            }

            throw e;
        }
    }

    private boolean checkPassword(String userId, String password) {
        ResponseEntity<AuthenticationResult> response = restTemplate.exchange(
                "http://authentication-service/authenticate",
                POST,
                new HttpEntity<>(new AuthenticationRequest(userId, password)),
                AuthenticationResult.class);

        return response.getBody().isSuccessful();
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }
}

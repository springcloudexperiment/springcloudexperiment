package springcloudexperiment.chatui;

import java.time.LocalDateTime;

public class Message {

    private String user;
    private String message;
    private LocalDateTime time;

    public Message() {
    }

    public Message(String user, String message, LocalDateTime time) {
        this.user = user;
        this.message = message;
        this.time = time;
    }

    public String getUser() {
        return user;
    }

    public String getMessage() {
        return message;
    }

    public LocalDateTime getTime() {
        return time;
    }

}

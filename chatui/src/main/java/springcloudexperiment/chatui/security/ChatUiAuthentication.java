package springcloudexperiment.chatui.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import springcloudexperiment.chatui.User;

import java.util.Collection;

public class ChatUiAuthentication extends UsernamePasswordAuthenticationToken {

    public ChatUiAuthentication(User user, Object credentials, Collection<? extends GrantedAuthority> authorities) {
        super(user, credentials, authorities);
    }

    @Override
    public String getName() {
        return ((User) this.getPrincipal()).getName();
    }
}

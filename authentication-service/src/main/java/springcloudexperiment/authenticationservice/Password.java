package springcloudexperiment.authenticationservice;

import org.springframework.data.annotation.Id;

public class Password {

    @Id
    private String id;

    private String userId;

    private String hash;
    private String salt;

    public Password() {
    }

    public Password(String userId, String hash, String salt) {
        this.id = id;
        this.userId = userId;
        this.hash = hash;
        this.salt = salt;
    }

    public String getHash() {
        return hash;
    }

    public String getSalt() {
        return salt;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }
}

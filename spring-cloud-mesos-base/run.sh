#!/usr/bin/env sh

echo "Setting up environment"
echo "----------------------"

export SERVER_PORT=$PORT0
echo "Set server port to $SERVER_PORT"

echo "Environment:"
env

JVM_ARGS="$JVM_ARGS -Deureka.instance.hostname=$HOSTNAME"
JVM_ARGS="$JVM_ARGS -Deureka.instance.ip-address=$LIBPROCESS_IP"
JVM_ARGS="$JVM_ARGS -Deureka.instance.instance-id=$MESOS_TASK_ID"

JVM_ARGS="$JVM_ARGS -Deureka.client.serviceUrl.defaultZone=$DISCOVERY_URI"

echo "JVM args:"
echo $JVM_ARGS

echo "Brace yourself! We're launching!"
java -Djava.security.egd=file:/dev/./urandom -jar $JVM_ARGS /app.jar
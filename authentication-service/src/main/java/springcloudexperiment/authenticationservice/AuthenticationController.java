package springcloudexperiment.authenticationservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class AuthenticationController {

    private Logger logger = LoggerFactory.getLogger(AuthenticationController.class);

    private PasswordRepository passwordRepository;
    private ShaPasswordEncoder passwordEncoder;
    private SaltGenerator saltGenerator;

    @Autowired
    public AuthenticationController(PasswordRepository passwordRepository, SaltGenerator saltGenerator, @Value("${iterations}") int iterations) {
        this.passwordRepository = passwordRepository;
        this.saltGenerator = saltGenerator;
        this.passwordEncoder = new ShaPasswordEncoder(512);
        this.passwordEncoder.setEncodeHashAsBase64(true);
        this.passwordEncoder.setIterations(iterations);

        logger.debug("Initialize AuthenticationController with {} iterations", iterations);
    }

    @RequestMapping(path = "/authenticate", method = POST)
    public AuthenticationResult authenticate(@RequestBody AuthenticationRequest request) {
        Optional<Password> password = passwordRepository.findPasswordByUserId(request.getUserId());

        boolean successful = password
                .map(p -> passwordEncoder.isPasswordValid(p.getHash(), request.getPassword(), p.getSalt()))
                .orElse(false);

        return new AuthenticationResult(successful);
    }

    @RequestMapping(path = "/register", method = POST)
    public void register(@RequestBody AuthenticationRequest request) {
        String salt = saltGenerator.generateSalt();

        Password password = new Password();
        password.setUserId(request.getUserId());
        password.setSalt(salt);
        password.setHash(passwordEncoder.encodePassword(request.getPassword(), salt));
        passwordRepository.save(password);
    }
}

package springcloudexperiment.authenticationservice;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;

import java.security.SecureRandom;

@Component
public class SaltGenerator {

    private int saltSize;

    public SaltGenerator(@Value("${salt-size:64}") int saltSize) {
        this.saltSize = saltSize;
    }

    public String generateSalt() {
        SecureRandom random = new SecureRandom();
        byte[] bytes = new byte[saltSize];
        random.nextBytes(bytes);
        return Base64Utils.encodeToString(bytes);
    }

}

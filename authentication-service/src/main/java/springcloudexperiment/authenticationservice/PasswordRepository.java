package springcloudexperiment.authenticationservice;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

public interface PasswordRepository extends CrudRepository<Password, String> {

    Optional<Password> findPasswordByUserId(String user);

}

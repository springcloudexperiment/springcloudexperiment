package springcloudexperiment.chatui;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class IndexController {

    private final RestTemplate restTemplate;

    @Autowired
    public IndexController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @RequestMapping("/")
    public String index(Model model) {
        ResponseEntity<PagedResources<Message>> response = restTemplate.exchange(
                "http://messages-service/messages",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<PagedResources<Message>>() {
                });

        model.addAttribute("username", getUsername());
        model.addAttribute("messages", response.getBody().getContent());

        return "index";
    }

    @RequestMapping(path = "/", method = POST)
    public String postMessage(@RequestBody MultiValueMap<String, String> formdata) {
        restTemplate.postForLocation("http://messages-service/messages", new Message(
                getUsername(),
                formdata.get("message").get(0),
                LocalDateTime.now()));
        return "redirect:/";
    }

    private String getUsername() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }
}